$(document).ready(function(){
  $('.produtos').owlCarousel({
    margin:10,
    responsive: {
      0: { items: 2},
      768: { items: 3},
      1024: { items: 4}
    },
    loop: true,
    autoplay:true,
    autoplayTimeout:1500,
    autoplayHoverPause:true
  });

  $('.testemunhal').owlCarousel({
    margin:10,
    loop: true,
    autoplay:true,
    autoplayTimeout:2500,
    autoplayHoverPause:true,
    responsive: {
      0: { items: 1},
      768: { items: 2},
      1024: { items: 3}
    }
  });
});