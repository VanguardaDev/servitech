<section id="contato" class="py-5">
    <div class="container">
        <h2 class="title">CONTATO</h2>
        <div class="row justify-content-between mt-5">
            <div class="col-md-6 col-lg-5">
                <div class="w-100 pb-5">
                    <p class='border-bottom'>
                        <span class="f-15 text-servitech bold">Loja Prime</span>
                        <a class="btn btn-whatsapp btn-sm rounded-pill mb-2 ml-4">
                            <span class="f-07 bold text-white">
                                QUERO FALAR AGORA
                                <i class="fab fa-whatsapp ml-1"></i>
                            </span>
                        </a>
                    </p>
                    <ul id='contact' class="list-group list-group-flush f-09">
                        <li class="list-group-item"><i class="fas fa-phone text-icon2 mr-1"></i>(92) 3633-3755</li>
                        <li class="list-group-item"><i class="fas fa-map-marker-alt text-icon2 mr-1"></i>Av. Djalma Batista, 315, Loja 4 - São Geraldo - Manaus - AM.</li>
                        <li class="list-group-item"><i class="fas fa-envelope text-icon2 mr-1"></i>
                        lojaprime@servitech-am.com.br
                        </li>
                    </ul>
                </div>
                <div class="w-100">
                    <p class='border-bottom'>
                        <span class="f-15 text-servitech bold">Mega Loja</span>
                        <a class="btn btn-whatsapp btn-sm rounded-pill mb-2 ml-4">
                            <span class="f-07 bold text-white">
                                QUERO FALAR AGORA
                                <i class="fab fa-whatsapp ml-1"></i>
                            </span>
                        </a>
                    </p>
                    <ul id='contact' class="list-group list-group-flush f-09">
                        <li class="list-group-item"><i class="fas fa-phone text-icon2 mr-1"></i>(92) 3633-1354</li>
                        <li class="list-group-item"><i class="fas fa-map-marker-alt text-icon2 mr-1"></i>Av. Visconde de Porto Alegre, 702 - Pç 14 de Janeiro - Manaus - AM.</li>
                        <li class="list-group-item"><i class="fas fa-envelope text-icon2 mr-1"></i>
                            megaloja@servitech-am.com.br
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-6 col-lg-5">
                <div class="w-100 bg-light rounded-20 p-5">
                    <h5 class="text-servitech bold text-uppercase">
                        fale conosco!
                    </h5>
                    <form>
                        <div class="form-group">
                            <input type="text" class="form-control contato"  placeholder="Nome">
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control contato"  placeholder="E-mail">
                        </div>
                        <div class="form-group">
                            <input type="tel" class="form-control contato"  placeholder="Celular">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control contato"  placeholder="Assunto">
                        </div>
                        <div class="form-group">
                            <textarea name="mensagem" cols='3' class='form-control contato'></textarea>
                        </div>
                        <div class="text-right">
                            <button type="button" name="" id="" class="btn btn-danger px-3 py-2">
                                <span class="bold text-uppercase">
                                    Enviar  
                                </span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>