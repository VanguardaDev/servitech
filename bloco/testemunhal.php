<section id="testemunhal" class="py-5">
<div class="container">
        <h2 class='text-uppercase title'>
            testemunhal
        </h2>
        <p class="lead mt-4 text-servitech text-center">
            O QUE OS CLIENTES ESTÃO DIZENDO SOBRE A SERVITECH
        </p>
        <div class="owl-carousel testemunhal owl-theme mb-5">
            <div class="w-100 pb-5">
                <div class="speech-bubble">
                    <p>O Lorem Ipsum é um texto modelo da indústria tipográfica e de impressão. O Lorem Ipsum tem vindo a ser o texto padrão usado por estas indústrias desde o ano de 1500...</p>
                    <p class='text-warning text-center mt-1 mb-0'>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star-half-alt"></i>
                    </p>
                </div>
                <div class="w-100 mt-5 mx-4">
                    <div class="row justify-content-center">
                        <div class="col-md-4">
                            <img src="<?= Url::getBase() ?>assets/images/circle.png" alt="" class="rounded-circle img-thumbnail">
                        </div>                  
                        <div class="col-md-6 text-secondary align-self-center">
                            <h6 class="bold">Rian Peek</h6>
                            <p class='f-08'>BMW Series 3</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="w-100 pb-5">
                <div class="speech-bubble">
                    <p>O Lorem Ipsum é um texto modelo da indústria tipográfica e de impressão. O Lorem Ipsum tem vindo a ser o texto padrão usado por estas indústrias desde o ano de 1500...</p>
                    <p class='text-warning text-center mt-1 mb-0'>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star-half-alt"></i>
                    </p>
                </div>
                <div class="w-100 mt-5 mx-4">
                    <div class="row justify-content-center">
                        <div class="col-md-4">
                            <img src="<?= Url::getBase() ?>assets/images/circle.png" alt="" class="rounded-circle img-thumbnail">
                        </div>                  
                        <div class="col-md-6 text-secondary align-self-center">
                            <h6 class="bold">Rian Peek</h6>
                            <p class='f-08'>BMW Series 3</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="w-100 pb-5">
                <div class="speech-bubble">
                    <p>O Lorem Ipsum é um texto modelo da indústria tipográfica e de impressão. O Lorem Ipsum tem vindo a ser o texto padrão usado por estas indústrias desde o ano de 1500...</p>
                    <p class='text-warning text-center mt-1 mb-0'>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star-half-alt"></i>
                    </p>
                </div>
                <div class="w-100 mt-5 mx-4">
                    <div class="row justify-content-center">
                        <div class="col-md-4">
                            <img src="<?= Url::getBase() ?>assets/images/circle.png" alt="" class="rounded-circle img-thumbnail">
                        </div>                  
                        <div class="col-md-6 text-secondary align-self-center">
                            <h6 class="bold">Rian Peek</h6>
                            <p class='f-08'>BMW Series 3</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="w-100 pb-5">
                <div class="speech-bubble">
                    <p>O Lorem Ipsum é um texto modelo da indústria tipográfica e de impressão. O Lorem Ipsum tem vindo a ser o texto padrão usado por estas indústrias desde o ano de 1500...</p>
                    <p class='text-warning text-center mt-1 mb-0'>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star-half-alt"></i>
                    </p>
                </div>
                <div class="w-100 mt-5 mx-4">
                    <div class="row justify-content-center">
                        <div class="col-md-4">
                            <img src="<?= Url::getBase() ?>assets/images/circle.png" alt="" class="rounded-circle img-thumbnail">
                        </div>                  
                        <div class="col-md-6 text-secondary align-self-center">
                            <h6 class="bold">Rian Peek</h6>
                            <p class='f-08'>BMW Series 3</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="w-100 pb-5">
                <div class="speech-bubble">
                    <p>O Lorem Ipsum é um texto modelo da indústria tipográfica e de impressão. O Lorem Ipsum tem vindo a ser o texto padrão usado por estas indústrias desde o ano de 1500...</p>
                    <p class='text-warning text-center mt-1 mb-0'>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star-half-alt"></i>
                    </p>
                </div>
                <div class="w-100 mt-5 mx-4">
                    <div class="row justify-content-center">
                        <div class="col-md-4">
                            <img src="<?= Url::getBase() ?>assets/images/circle.png" alt="" class="rounded-circle img-thumbnail">
                        </div>                  
                        <div class="col-md-6 text-secondary align-self-center">
                            <h6 class="bold">Rian Peek</h6>
                            <p class='f-08'>BMW Series 3</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="w-100 pb-5">
                <div class="speech-bubble">
                    <p>O Lorem Ipsum é um texto modelo da indústria tipográfica e de impressão. O Lorem Ipsum tem vindo a ser o texto padrão usado por estas indústrias desde o ano de 1500...</p>
                    <p class='text-warning text-center mt-1 mb-0'>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star-half-alt"></i>
                    </p>
                </div>
                <div class="w-100 mt-5 mx-4">
                    <div class="row justify-content-center">
                        <div class="col-md-4">
                            <img src="<?= Url::getBase() ?>assets/images/circle.png" alt="" class="rounded-circle img-thumbnail">
                        </div>                  
                        <div class="col-md-6 text-secondary align-self-center">
                            <h6 class="bold">Rian Peek</h6>
                            <p class='f-08'>BMW Series 3</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>