<section id="text-destaque">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-5 col-lg-6 d-none d-lg-block">
                <img src="https://via.placeholder.com/810x400.png" alt="">
            </div>
            <div class="col-lg-6 col-xl-7 col-md-12 bg-servitech-3">
                <div class="align-self-center">
                    <div id='destaque'>
                        <h1 class="text-white bold mb-2">
                            TEXTO EM DESTAQUE
                        </h1>
                        <p class="text-white">
                            A satisfação completa do cliente é o objetivo principal da Servitech e o que guia todas as suas ações. Trabalhamos com uma equipe de atendimento altamente treinada, para garantir 100% de satisfação do cliente.
                        </p>
                        <a name="" id="" class="btn btn-danger" href="#" role="button">QUERO AGENDAR</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>