<section id="newsletter" class="py-5 bg-light">
    <div class="container">
        <h3 class="text-uppercase title pb-5">
            novidades
        </h3>
        <div class="row justify-content-center">
            <div class="col-md-9">
                <h5 class="text-center text-servitech mt-3">
                    INSCREVA-SE PARA RECEBER NOVIDADES
                </h5>
                <p class='text-center text-servitech my-4'>
                    Fique por dentro das nossas novidades e receba informações sobre produtos, serviços e promoções.
                </p>
                <div class="input-group mt-3">
                    <input type="text" class="form-control" placeholder="Seu e-mail..." aria-label="Seu e-mail..." aria-describedby="button-addon2">
                    <div class="input-group-append">
                        <button class="btn btn-danger px-5" type="button" id="button-addon2">CADASTRAR</button>
                    </div>
                </div>
                <div id="whatsapp" class="pt-5 text-center">
                    <p class='text-servitech'>Quer falar agora com a gente? é so clicar abaixo:</p>
                    <a class="btn btn-whatsapp px-4 py-2 rounded-pill">
                        <span class="f-14 bold text-white">
                            QUERO FALAR AGORA
                            <i class="fab fa-whatsapp ml-1"></i>
                        </span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>