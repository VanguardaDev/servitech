<header id='header' class='bg-dark py-3 d-none d-lg-block fixed-top'>
    <div class="container">
        <div class="row justify-content-between">
            <div class="col-lg-3">
                <a class="navbar-brand" href="<?php echo URL::getBase() ?>">
                    <img src="<?= URL::getBase() ?>assets/images/logo_branca-200x53.png" class='img-fluid mt-2'>
                </a>
            </div>
            <div class="col-lg-8 col-xl-7">
                <div class="w-100">
                    <ul id='head-text' class="list-inline text-white border-bottom pb-1 text-center">
                        <li class="list-inline-item f-08">
                            <i class="fas fa-phone-square mr-1"></i>
                            +55 92 9 9888.7654
                        </li>
                        <li class="list-inline-item f-08">
                            <i class="fas fa-envelope mr-1"></i>
                            <a href="mailto:contato@servitech-am.com.br" target='_blank'>
                                contato@servitech-am.com.br
                            </a>
                        </li>
                        <li class="list-inline-item f-08">
                            <i class="fas fa-clock mr-1"></i>
                            Seg - Sáb 9h às 18h
                        </li>
                        <li class="list-inline-item f-08" style='margin-right: .2rem;'>
                            <a href="" target='_blank'>
                                <i class="fab fa-facebook-f"></i>
                            </a>
                        </li>
                        <li class="list-inline-item f-08">
                            <a href="" target='_blank'>
                                <i class="fab fa-instagram"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="w-100" id='nav-menu'>
                    <ul class="nav justify-content-center">
                        <li class="nav-item">
                            <a class="nav-link b-6" href="<?php echo URL::getBase() ?>">INICIO</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link b-6" href="<?php echo URL::getBase() ?>home#a-empresa">A EMPRESA</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link b-6" href="<?php echo URL::getBase() ?>produtos">PRODUTOS</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link b-6" href="<?php echo URL::getBase() ?>servicos">SERVIÇOS</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link b-6" href="<?php echo URL::getBase() ?>#contato">CONTATO</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>

<header class='d-md-block d-lg-none py-2 bg-dark'>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="#">
            <img src="<?= URL::getBase() ?>assets/images/logo_branca-200x53.png" width='120px'>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="nav-menu">
            <ul class="navbar-nav justify-content-center">
                <li class="nav-item">
                    <a class="nav-link b-6" href="#home">INICIO</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link b-6" href="#">A EMPRESA</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link b-6" href="#">PRODUTOS</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link b-6" href="#">SERVIÇOS</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link b-6" href="#">CONTATO</a>
                </li>
            </ul>
        </div>
    </nav>
</header>