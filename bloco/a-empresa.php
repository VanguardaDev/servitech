<section id="a-empresa" class="my-5">
    <div class="container">
        <h2 class='title'>A SERVITECH</h2>
        <div class="row mt-5">
            <div class="col-lg-7 col-md-6">
                <p class="lead text-servitech">
                    A empresa
                </p>
                <h4 class="b-7">
                    QUAIS AS VANTAGENS DE ESCOLHER A SERVITECH?
                </h4>
                <p class="text-servitech f-09">
                    A facilidade de encontrar todos os produtos que deseja no mesmo lugar e comprá-los na comodidade da sua casa ou trabalho. Tendo como compromisso oferecer ótimo atendimento e produtos exclusivos para atender à todos.
                </p>
                <div class="row">
                    <div class="col-lg-6">
                        <h5 class="bold">Produtos</h5>
                        <ul id='aempresa' class="list-group list-group-flush f-09">
                            <li class="list-group-item"><i class="fas fa-chevron-circle-right text-icon mr-1"></i>Turbine.</li>
                            <li class="list-group-item"><i class="fas fa-chevron-circle-right text-icon mr-1"></i>Sonzeira.</li>
                            <li class="list-group-item"><i class="fas fa-chevron-circle-right text-icon mr-1"></i>Segurança.</li>
                            <li class="list-group-item"><i class="fas fa-chevron-circle-right text-icon mr-1"></i>Conforto e Comodidade.</li>
                            <li class="list-group-item"><i class="fas fa-chevron-circle-right text-icon mr-1"></i>Soluções Multimídias.</li>
                            <li class='list-group-item'><i class="fas fa-chevron-circle-right text-icon mr-1"></i>Teto Solar.</li>
                            <li class='list-group-item'><i class="fas fa-chevron-circle-right text-icon mr-1"></i>Revestimento em couro e corvin.</li>
                        </ul>
                    </div>
                    <div class="col-lg-6">
                        <h5 class="bold">Serviços</h5>
                        <ul id='aempresa' class="list-group list-group-flush f-09">
                            <li class="list-group-item"><i class="fas fa-chevron-circle-right text-icon mr-1"></i>Higienização Interna.</li>
                            <li class="list-group-item"><i class="fas fa-chevron-circle-right text-icon mr-1"></i>Hitradação de Banco de Couro.</li>
                            <li class="list-group-item"><i class="fas fa-chevron-circle-right text-icon mr-1"></i>Lavagem a Seco.</li>
                            <li class="list-group-item"><i class="fas fa-chevron-circle-right text-icon mr-1"></i>Lavagem de Motor.</li>
                            <li class="list-group-item"><i class="fas fa-chevron-circle-right text-icon mr-1"></i>Polimento Cristalizado.</li>
                            <li class='list-group-item'><i class="fas fa-chevron-circle-right text-icon mr-1"></i>Esterilização de ar condicionado.</li>
                            <li class='list-group-item'><i class="fas fa-chevron-circle-right text-icon mr-1"></i>Impermeabilização de Bancos.</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 col-md-6">
                <img src="https://via.placeholder.com/360x420.png" alt="" class='d-none d-md-block'>
                <img src="https://via.placeholder.com/340x420.png" alt="" class='d-sm-block d-md-none'>
                <div class="pt-md-4 text-servitech d-md-block d-lg-none">
                    <h6 class='bold'>VAMOS CONHECER UM POUCO DA SERVITECH?</h6>
                    <p>Foi criada em 2005 em Manaus, no Amazonas. O seu conhecimento aliado à produtos de alta qualidade são uma das suas principais características. Veja a seguir uma das razões que levam a Servitech ser preferência da maioria dos consumidores.</p>
                </div>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-lg-12 text-servitech">
                <h6 class='bold d-lg-none'>VAMOS CONHECER UM POUCO DA SERVITECH?</h6>
                <p class='d-lg-none'>Foi criada em 2005 em Manaus, no Amazonas. O seu conhecimento aliado à produtos de alta qualidade são uma das suas principais características. Veja a seguir uma das razões que levam a Servitech ser preferência da maioria dos consumidores.</p>
                <h6 class='bold mt-4'>POR QUE INVESTIR NOS PRODUTOS SERVITECH?</h6>
                <p>É reconhecida por seus principais fornecedores como empresa autorizada a revender produtos de suas marcas na cidade de Manaus e pela internet, como: Focal, Hertz, Audiophonic, DAT, JBL, Selenium, Db Drive, Webasto, Keko, entre outras. Oferece uma enorme variedade de produtos para carros, dentre eles banco de couro, som automotivo de alta qualidade, acessórios para pick-ups e teto-solar.</p>
                <h6 class='bold mt-4'>AS COMPRAS SÃO SEGURAS PELO SITE?</h6>
                <p>Com a utilização de tecnologia moderna, os dados estão altamente seguros, portanto todos os produtos vendidos online estão dentro da lei e são originais de fábrica para a segurança da compra e investimento.</p>
                <h6 class='bold mt-4'>BUSCA ATENDIMENTO PERSONALIZADO?</h6>
                <p>Com a melhor orientação para uma compra inteligente, a Servitech possui equipe altamente treinada para satisfação 100% do cliente. As informações são de extrema qualidade, com o melhor serviço, e atendimento exclusivo.</p>
                <p class="bold">Servitech, atendendo sempre sua expectativa.</p>
            </div>
        </div>
    </div>
</section>
       