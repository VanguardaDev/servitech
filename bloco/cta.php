<section id="cta" class="bg-servitech-2">
    <div class="container-fluid">
    <div class="row justify-content-center py-4">
        <div class="col-lg-5">
            <div class="bg-cta d-flex align-items-center">
                <div class="w-100 row justify-content-end mt-3">
                    <div class="col-lg-7 col-md-7">
                        <h6 class='text-servitech bold text-right'>AGENDE UM SERVIÇO COM A GENTE!</h6>
                    </div>
                    <div class="col-md-3 col-lg-4 col-xl-5">
                        <a name="" id="" class="btn btn-info" href="#" role="button">AGENDAR</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-5">
            <div class="row pt-md-4">
                <div class="col-md-6">
                    <h6 class='text-servitech bold border-bottom'>
                        Loja Prime
                    </h6>
                    <ul id='cta' class="list-group list-group-flush f-08">
                        <li class="list-group-item"><i class="fas fa-phone text-icon2 mr-1"></i>(92) 3633-3755</li>
                        <li class="list-group-item"><i class="fas fa-map-marker-alt text-icon2 mr-1"></i>Av. Djalma Batista, 315, Loja 4 - São Geraldo - Manaus - AM.</li>
                        <li class="list-group-item"><i class="fas fa-envelope text-icon2 mr-1"></i>
                        lojaprime@servitech-am.com.br
                        </li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <h6 class='text-servitech bold border-bottom'>
                        Mega Loja
                    </h6>
                    <ul id='cta' class="list-group list-group-flush f-08">
                        <li class="list-group-item"><i class="fas fa-phone text-icon2 mr-1"></i>(92) 3633-1354</li>
                        <li class="list-group-item"><i class="fas fa-map-marker-alt text-icon2 mr-1"></i>Av. Visconde de Porto Alegre, 702 - Pç 14 de Janeiro - Manaus - AM.</li>
                        <li class="list-group-item"><i class="fas fa-envelope text-icon2 mr-1"></i>
                            megaloja@servitech-am.com.br
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>