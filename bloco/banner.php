<section id="home">
    <div class="view d-flex align-items-center">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <img src="<?= Url::getBase() ?>assets/images/head-car.png" class='img-fluid' alt="">
                </div>
                <div class="col-lg-5 col-md-6 mt-5">
                    <h2 class="text-white text-shadow b-6 sp">
                    <span class='bold f-25'>SERVITECH</span>, atendendo sempre sua expectativa.
                    </h2>
                </div>
            </div>
        </div>
    </div>
</section>
