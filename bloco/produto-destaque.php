<section id="produto-destaque" class="bg-servitech-2 py-5">
    <div class="container">
        <h2 class='text-uppercase title'>
            produtos em destaque
        </h2>
        <p class="lead mt-4 text-servitech text-center">
            Conheça nossa linha completa de produtos e serviços
        </p>
        <div class="owl-carousel produtos owl-theme">
            <div class="w-100 bg-white text-center">
                <img src="https://via.placeholder.com/255x264.png" alt="">
                <p class="text-servitech text-uppercase py-2">
                    nome do produto
                </p>
            </div>
            <div class="w-100 bg-white text-center">
                <img src="https://via.placeholder.com/255x264.png" alt="">
                <p class="text-servitech text-uppercase py-2">
                    nome do produto
                </p>
            </div>
            <div class="w-100 bg-white text-center">
                <img src="https://via.placeholder.com/255x264.png" alt="">
                <p class="text-servitech text-uppercase py-2">
                    nome do produto
                </p>
            </div>
            <div class="w-100 bg-white text-center">
                <img src="https://via.placeholder.com/255x264.png" alt="">
                <p class="text-servitech text-uppercase py-2">
                    nome do produto
                </p>
            </div>
            <div class="w-100 bg-white text-center">
                <img src="https://via.placeholder.com/255x264.png" alt="">
                <p class="text-servitech text-uppercase py-2">
                    nome do produto
                </p>
            </div>
            <div class="w-100 bg-white text-center">
                <img src="https://via.placeholder.com/255x264.png" alt="">
                <p class="text-servitech text-uppercase py-2">
                    nome do produto
                </p>
            </div>
        </div>
        <div class="text-center pt-4">
            <a name="" id="" class="btn btn-danger py-2 px-3" href="#" role="button">QUERO CONHECER</a>
        </div>
    </div>
</section>