<h2 class="title text-uppercase">HIDRATAÇÃO DE BANCO DE COURO</h2>
<p class="lead sub-italic mb-1">
Com a exposição aos raios solares, resulta do ressecamento do couro, rachando e perdendo características naturais.
</p>
<p class='f-12 text-center text-danger bold'>
Faça a limpeza e hidratação do couro a cada 4 ou 6 meses.
</p>
<div class="row">
    <div class="col-md-4 col-lg-3 col-6 mb-4">
        <a href="#">
            <img src="http://via.placeholder.com/250x250" alt="">
        </a>
    </div>
    <div class="col-md-4 col-lg-3 col-sm-6 mb-4">
        <a href="#">
            <img src="http://via.placeholder.com/250x250" alt="">
        </a>
    </div>
    <div class="col-md-4 col-lg-3 col-sm-6 mb-4">
        <a href="#">
            <img src="http://via.placeholder.com/250x250" alt="">
        </a>
    </div>
    <div class="col-md-4 col-lg-3 col-sm-6 mb-4">
        <a href="#">
            <img src="http://via.placeholder.com/250x250" alt="">
        </a>
    </div>
    <div class="col-md-4 col-lg-3 col-sm-6 mb-4">
        <a href="#">
            <img src="http://via.placeholder.com/250x250" alt="">
        </a>
    </div>
    <div class="col-md-4 col-lg-3 col-sm-6 mb-4">
        <a href="#">
            <img src="http://via.placeholder.com/250x250" alt="">
        </a>
    </div>
    <div class="col-md-4 col-lg-3 col-sm-6 mb-4">
        <a href="#">
            <img src="http://via.placeholder.com/250x250" alt="">
        </a>
    </div>
    <div class="col-md-4 col-lg-3 col-sm-6 mb-4">
        <a href="#">
            <img src="http://via.placeholder.com/250x250" alt="">
        </a>
    </div>
</div>
<div class="text-center py-4">
    <a class="btn btn-whatsapp px-4 py-2 rounded-pill">
        <span class="f-14 bold text-white">
            QUERO AGENDAR
            <i class="fab fa-whatsapp ml-1"></i>
        </span>
    </a>
</div>