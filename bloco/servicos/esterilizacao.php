<h2 class="title text-uppercase">ESTERILAZAÇÃO DE AR CONDICIONADO</h2>
<p class="lead sub-italic mb-1">
Serviço para a eliminação de mofo, fungos, ácaros e bactérias e mau cheiro. - Indispensável para pessoas alérgicas; Recomendável o serviço completo com a extensão total a todo o receptáculo interno do veículo, incluindo estofados e laterais de portas (se em tecido) e carpete. - Aplicável a dutos de ar-condicionado, com opção de inclusão da troca do filtro do ar-condicionado (exceto nos carros que não possuem filtro)
</p>
<div class="row">
    <div class="col-md-4 col-lg-3 col-6 mb-4">
        <a href="#">
            <img src="http://via.placeholder.com/250x250" alt="">
        </a>
    </div>
    <div class="col-md-4 col-lg-3 col-sm-6 mb-4">
        <a href="#">
            <img src="http://via.placeholder.com/250x250" alt="">
        </a>
    </div>
    <div class="col-md-4 col-lg-3 col-sm-6 mb-4">
        <a href="#">
            <img src="http://via.placeholder.com/250x250" alt="">
        </a>
    </div>
    <div class="col-md-4 col-lg-3 col-sm-6 mb-4">
        <a href="#">
            <img src="http://via.placeholder.com/250x250" alt="">
        </a>
    </div>
    <div class="col-md-4 col-lg-3 col-sm-6 mb-4">
        <a href="#">
            <img src="http://via.placeholder.com/250x250" alt="">
        </a>
    </div>
    <div class="col-md-4 col-lg-3 col-sm-6 mb-4">
        <a href="#">
            <img src="http://via.placeholder.com/250x250" alt="">
        </a>
    </div>
    <div class="col-md-4 col-lg-3 col-sm-6 mb-4">
        <a href="#">
            <img src="http://via.placeholder.com/250x250" alt="">
        </a>
    </div>
    <div class="col-md-4 col-lg-3 col-sm-6 mb-4">
        <a href="#">
            <img src="http://via.placeholder.com/250x250" alt="">
        </a>
    </div>
</div>
<div class="text-center py-4">
    <a class="btn btn-whatsapp px-4 py-2 rounded-pill">
        <span class="f-14 bold text-white">
            QUERO AGENDAR
            <i class="fab fa-whatsapp ml-1"></i>
        </span>
    </a>
</div>