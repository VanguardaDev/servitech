<h2 class="title text-uppercase">LAVAGEM A SECO</h2>
<p class="lead sub-italic mb-1">
Processo limpo e silencioso, sem contaminar o meio ambiente. Sistema de lavagem ecológica. - Não se utiliza água - Baseada na aplicação de cera líquida biodegradável de alta tecnologia.
</p>
<p class="lead sub-italic">
Limpa e encera o carro simultaneamente. - Sem perigo de riscá-lo - Lavagem ecológica.
</p>
<div class="row">
    <div class="col-md-4 col-lg-3 col-6 mb-4">
        <a href="#">
            <img src="http://via.placeholder.com/250x250" alt="">
        </a>
    </div>
    <div class="col-md-4 col-lg-3 col-sm-6 mb-4">
        <a href="#">
            <img src="http://via.placeholder.com/250x250" alt="">
        </a>
    </div>
    <div class="col-md-4 col-lg-3 col-sm-6 mb-4">
        <a href="#">
            <img src="http://via.placeholder.com/250x250" alt="">
        </a>
    </div>
    <div class="col-md-4 col-lg-3 col-sm-6 mb-4">
        <a href="#">
            <img src="http://via.placeholder.com/250x250" alt="">
        </a>
    </div>
    <div class="col-md-4 col-lg-3 col-sm-6 mb-4">
        <a href="#">
            <img src="http://via.placeholder.com/250x250" alt="">
        </a>
    </div>
    <div class="col-md-4 col-lg-3 col-sm-6 mb-4">
        <a href="#">
            <img src="http://via.placeholder.com/250x250" alt="">
        </a>
    </div>
    <div class="col-md-4 col-lg-3 col-sm-6 mb-4">
        <a href="#">
            <img src="http://via.placeholder.com/250x250" alt="">
        </a>
    </div>
    <div class="col-md-4 col-lg-3 col-sm-6 mb-4">
        <a href="#">
            <img src="http://via.placeholder.com/250x250" alt="">
        </a>
    </div>
</div>
<div class="text-center py-4">
    <a class="btn btn-whatsapp px-4 py-2 rounded-pill">
        <span class="f-14 bold text-white">
            QUERO AGENDAR
            <i class="fab fa-whatsapp ml-1"></i>
        </span>
    </a>
</div>