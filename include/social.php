<a href="https://www.facebook.com/lacquaresidenzaalianca/" target="_blank">
	<i class="fa fa-facebook" aria-hidden="true"></i>
</a>
<a href="https://www.instagram.com/lacquaresidenza/?utm_source=ig_profile_share&igshid=1mu3hjhazd0gp" target="_blank">
	<i class="fa fa-instagram" aria-hidden="true"></i>
</a>

<a href="mailto:contato@lacquaresidenza.com.br" target="_blank">
	<i class="fa fa-envelope-o" aria-hidden="true"></i>
</a>