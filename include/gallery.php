<div class="row mt-5">
	<div class="col-sm-3 my-3">
		<img class="rounded-circle img-fluid fs-gal" src="<?php echo URL::getBase() ?>assets/images/gallery/lacqua-1-thumb.jpeg" data-url="<?php echo URL::getBase() ?>assets/images/gallery/lacqua-1.jpeg" alt="Quadra Poliesportiva e de Tenis">
	</div>
	<div class="col-sm-3 my-3">
		<img class="rounded-circle img-fluid fs-gal" src="<?php echo URL::getBase() ?>assets/images/gallery/lacqua-2-thumb.jpeg"  data-url="<?php echo URL::getBase() ?>assets/images/gallery/lacqua-2.jpeg" alt="Parque aquatico Piscina Adulto">
	</div>
	<div class="col-sm-3 my-3">
		<img class="rounded-circle img-fluid fs-gal" src="<?php echo URL::getBase() ?>assets/images/gallery/lacqua-3-thumb.jpeg"  data-url="<?php echo URL::getBase() ?>assets/images/gallery/lacqua-3.jpeg" alt="Salão de festa Infantil">
	</div>
	<div class="col-sm-3 my-3">
		<img class="rounded-circle img-fluid fs-gal" src="<?php echo URL::getBase() ?>assets/images/gallery/lacqua-4-thumb.jpeg"  data-url="<?php echo URL::getBase() ?>assets/images/gallery/lacqua-4.jpeg" alt="Parque Aquatico">
	</div>
	<div class="col-sm-3 my-3">
		<img class="rounded-circle img-fluid fs-gal" src="<?php echo URL::getBase() ?>assets/images/gallery/lacqua-5-thumb.jpeg"  data-url="<?php echo URL::getBase() ?>assets/images/gallery/lacqua-5.jpeg" alt="Churrasqueira">
	</div>
	<div class="col-sm-3 my-3">
		<img class="rounded-circle img-fluid fs-gal" src="<?php echo URL::getBase() ?>assets/images/gallery/lacqua-6-thumb.jpeg"  data-url="<?php echo URL::getBase() ?>assets/images/gallery/lacqua-6.jpeg" alt="Salão de festas Adulto">
	</div>
	<div class="col-sm-3 my-3">
		<img class="rounded-circle img-fluid fs-gal" src="<?php echo URL::getBase() ?>assets/images/gallery/lacqua-7-thumb.jpeg"  data-url="<?php echo URL::getBase() ?>assets/images/gallery/lacqua-7.jpeg" alt="Parque Aquatico Piscina Infantil">
	</div>
	<div class="col-sm-3 my-3">
		<img class="rounded-circle img-fluid fs-gal" src="<?php echo URL::getBase() ?>assets/images/gallery/lacqua-8-thumb.jpeg"  data-url="<?php echo URL::getBase() ?>assets/images/gallery/lacqua-8.jpeg" alt="Espaço Fitnes">
	</div>
</div>


<!-- Full screen gallery. -->
<div class="fs-gal-view">
	<h1></h1>
	<img class="fs-gal-prev fs-gal-nav" src="<?php echo URL::getBase() ?>assets/images/prev.svg" alt="Previous picture" title="Previous picture" />
	<img class="fs-gal-next fs-gal-nav" src="<?php echo URL::getBase() ?>assets/images/next.svg" alt="Next picture" title="Next picture" />
	<img class="fs-gal-close" src="<?php echo URL::getBase() ?>assets/images/close.svg" alt="Close gallery" title="Close gallery" />
	<img class="fs-gal-main" src="" alt="" />
</div>