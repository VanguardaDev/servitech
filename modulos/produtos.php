<section id="produtos" class="pt-5">
    <div class="container">
        <?php require 'bloco/produtos/turbine.php' ?>
        <?php require 'bloco/produtos/sonzeira.php' ?>
        <?php require 'bloco/produtos/securanca.php' ?>
        <?php require 'bloco/produtos/conforto.php' ?>
        <?php require 'bloco/produtos/multimidias.php' ?>
        <?php require 'bloco/produtos/teto-solar.php' ?>
        <?php require 'bloco/produtos/revestimento.php' ?>
    </div>
</section>
<?php require 'bloco/newsletter.php' ?>
<?php require 'bloco/contact.php' ?>