<div class="mtop-page container page404">
	<div class="row">
		<div class="col-sm-4">
			<img  src="<?php echo URL::getBase() ?>assets/images/404.png" alt="404">
		</div>
		<div class="col-sm-8">
			<h1 class="h1-responsive">Página não encontrada</h1>
			<br>
			<h5>Não conseguimos encontrar a página que você está procurando.</h5>
			<h5>Você pode retornar à página anterior ou entrar em contato com nossa equipe de suporte.</h5>
			<br>
			<br>
			<a href="<?php echo URL::getBase() ?>" class="btn btn-primary btn-sm mr-3">Página principal</a>
			<a href="javascript:history.back()" class="btn btn-secondary btn-sm">Voltar</a>
		</div>
	</div>
	
</div>