<?php 

$array = explode('/', $_SERVER['REQUEST_URI']);
$id =  $array[3];
$carro = Rest::getFindBy('carros', 'id', $id, 1); 
print_r($carro);

foreach($carro as $k => $v) {
    echo "\$carro[$k] => $v.\n";
}
?>
<section id="detalhes" class='py-5'>
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <h2 class="text-center border-bottom pb-2 text-uppercase bold">
                </h2>
                <div class="row">
                    <div class="col-md-8">
                        <div class="bd-example">
                            <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
                                <ol class="carousel-indicators">
                                    <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
                                    <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
                                    <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
                                </ol>
                                <div class="carousel-inner">
                                    <div class="carousel-item active">
                                        <img src="http://placehold.it/600x300?text=One" class="d-block w-100" alt="...">
                                        <div class="carousel-caption d-none d-md-block">
                                            <h5>First slide label</h5>
                                            <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                                        </div>
                                    </div>
                                    <div class="carousel-item">
                                        <img src="http://placehold.it/600x300?text=Two" class="d-block w-100" alt="...">
                                        <div class="carousel-caption d-none d-md-block">
                                            <h5>Second slide label</h5>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                        </div>
                                    </div>
                                    <div class="carousel-item">
                                        <img src="http://placehold.it/600x300?text=Three" class="d-block w-100" alt="...">
                                        <div class="carousel-caption d-none d-md-block">
                                            <h5>Third slide label</h5>
                                            <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
                                        </div>
                                    </div>
                                </div>
                                    <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 mt-4">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item py-1">
                                <img src="./assets/icons/location-18x18.png" class='img-fluid mr-3' alt="">
                                <span class='align-middle f-09'>Estado: Amazonas</span>
                            </li>
                            <li class="list-group-item py-1">
                                <img src="./assets/icons/location-18x18.png" class='img-fluid mr-3' alt="">
                                <span class='align-middle f-09'>Cidade: Manaus</span>
                            </li>
                            <li class="list-group-item py-1">
                                <img src="./assets/icons/cor-18x18.png" class='img-fluid mr-3' alt="">
                                <span class='align-middle f-09'>Cor: 4</span>
                            </li>
                            <li class="list-group-item py-1">
                                <img src="./assets/icons/year-18x18.png" class='img-fluid mr-3' alt="">
                                <span class='align-middle f-09'>Ano: 4</span>
                            </li>
                            <li class="list-group-item py-1">
                                <img src="./assets/icons/kilometragem-18x18.png" class='img-fluid mr-3' alt="">
                                <span class='align-middle f-09'>Kilometragem: 4</span>
                            </li>
                            <li class="list-group-item py-1">
                                <img src="./assets/icons/conbustivel-18x18.png" class='img-fluid mr-3' alt="">
                                <span class='align-middle f-09'>Comb.: 4</span>
                            </li>
                            <li class="list-group-item py-1">
                                <img src="./assets/icons/cambios-18x18.png" class='img-fluid mr-3' alt="">
                                <span class='align-middle f-09'>Cambios: 4</span>
                            </li>
                            <li class="list-group-item py-1">
                                <img src="./assets/icons/portas-18x18.png" class='img-fluid mr-3' alt="">
                                <span class='align-middle f-09'>Portas: 4</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <?php require 'bloco/lead-sidebar.php'; ?>
            </div>
        </div>
        <div class="row">
            
        </div>
    </div>
</section>
<?php require 'bloco/ofertas-imperdivel.php'; ?>
<?php require 'bloco/banner.php'; ?>
<?php require 'bloco/ofertas-random.php'; ?>