<section id="produtos" class="pt-5">
    <div class="container">
        <?php require 'bloco/servicos/higienizacao.php'; ?>
        <?php require 'bloco/servicos/hidratacao.php'; ?>
        <?php require 'bloco/servicos/lavagem-seco.php'; ?>
        <?php require 'bloco/servicos/lavagem-motor.php'; ?>
        <?php require 'bloco/servicos/polimento.php'; ?>
        <?php require 'bloco/servicos/esterilizacao.php'; ?>
        <?php require 'bloco/servicos/impermeabilizacao.php'; ?>
    </div>
</section>
<?php require 'bloco/newsletter.php'; ?>
<?php require 'bloco/contact.php'; ?>